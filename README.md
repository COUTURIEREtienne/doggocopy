# Love My Doggies

Site d'adoption de chiens, parce qu'on aime les chiens !

## Installation du projet

- [ ] Cloner le projet depuis [https://gitlab.com/drakona-formation/lovemydoggies](https://gitlab.com/drakona-formation/lovemydoggies)
- [ ] Aller dans le dossier et lancer `composer install`
