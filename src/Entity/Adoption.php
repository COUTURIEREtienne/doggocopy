<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Traits\HasIdTrait;
use App\Entity\Traits\HasNameTrait;
use App\Repository\AdoptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=AdoptionRepository::class)
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"},
 *     attributes={
 *         "order"={"updatedAt": "DESC"}
 *     },
 *     paginationItemsPerPage=1
 * )
 * @ApiFilter(SearchFilter::class, properties={"dogs.id": "exact", "dogs.name": "partial"})
 */
class Adoption
{
    use HasIdTrait;
    use HasNameTrait;
    use TimestampableEntity;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="adoption")
     * @ApiSubresource(
     *     maxDepth=2
     * )
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity=Dog::class, mappedBy="adoption", cascade={"persist"})
     * @ApiSubresource(
     *     maxDepth=2
     * )
     */
    private $dogs;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="adoptions")
     * @ApiSubresource(
     *     maxDepth=2
     * )
     */
    private $user;


    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->dogs = new ArrayCollection();
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
            $contact->setAdoption($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getAdoption() === $this) {
                $contact->setAdoption(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Dog[]
     */
    public function getDogs(): Collection
    {
        return $this->dogs;
    }

    public function addDog(Dog $dog): self
    {
        if (!$this->dogs->contains($dog)) {
            $this->dogs[] = $dog;
            $dog->setAdoption($this);
        }

        return $this;
    }

    public function removeDog(Dog $dog): self
    {
        if ($this->dogs->removeElement($dog)) {
            // set the owning side to null (unless already changed)
            if ($dog->getAdoption() === $this) {
                $dog->setAdoption(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        $string = $this->getName();

        if (!empty($this->getDogs())) {
            $string .= ' (';
            foreach ($this->getDogs() as $key => $dog) {
                $string .= $dog->getName();
                if ($key < count($this->getDogs()) - 1) {
                    $string .= ', ';
                }
            }
            $string .= ')';
        }

        return $string;
    }

    public function isClosed(): bool
    {
        foreach ($this->getDogs() as $dog) {
            if (!$dog->getTaken()) {
                return false;
            }
        }

        return true;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
