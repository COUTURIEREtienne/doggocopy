<?php

namespace App\Controller;

use App\Entity\Adoption;
use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\AdoptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default_index")
     */
    public function index(AdoptionRepository $adoptionRepository): Response
    {
        $adoptions = $adoptionRepository->findNotTaken(4, true);

        return $this->render('default/index.html.twig', [
            'adoptions' => $adoptions,
        ]);
    }

    /**
     * @Route("/contact", name="default_contact")
     * @Route("/contact/{id}", name="default_contact_for_adoption")
     */
    public function contact(Request $request, EntityManagerInterface $entityManager, Adoption $adoption = null): Response
    {
        $contact = new Contact();
        $contact->setAdoption($adoption);

        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($contact);
            $entityManager->flush();

            $this->addFlash('success', 'Votre message a bien été envoyé');

            return $this->redirectToRoute('default_contact');
        }

        return $this->render('default/contact.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/conditions", name="default_conditions")
     */
    public function conditions(): Response
    {
        return $this->render('default/conditions.html.twig');
    }
}
