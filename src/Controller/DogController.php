<?php

namespace App\Controller;

use App\Entity\Dog;
use App\Entity\User;
use App\Form\DogType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/chien")
 */
class DogController extends AbstractController
{
    /**
     * @Route("/nouveau", name="dog_new")
     * @Route("/{id}/modifier", name="dog_edit")
     * @IsGranted("ROLE_USER")
     */
    public function form(Request $request, EntityManagerInterface $em, ?Dog $dog = null): Response
    {
        if (empty($dog)) {
            $dog = new Dog();
        }

        $form = $this->createForm(DogType::class, $dog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getUser();
            $dog->setUser($user);
            $em->persist($dog);
            $em->flush();

            return $this->redirectToRoute('default_index');
        }

        return $this->render('dog/form.html.twig', [
            'dog'  => $dog,
            'form' => $form->createView(),
        ]);
    }
}
