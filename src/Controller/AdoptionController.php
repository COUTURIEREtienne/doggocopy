<?php

namespace App\Controller;

use App\Entity\Adoption;
use App\Entity\User;
use App\Form\AdoptionType;
use App\Repository\AdoptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/annonces")
 */
class AdoptionController extends AbstractController
{
    /**
     * @Route("/", name="adoption_index")
     */
    public function index(AdoptionRepository $adoptionRepository): Response
    {
        $adoptions = $adoptionRepository->findNotTaken();

        return $this->render('adoption/index.html.twig', [
            'adoptions' => $adoptions,
        ]);
    }

    /**
     * @Route("/nouvelle", name="adoption_new")
     * @Route("/{id}/modifier", name="adoption_edit")
     * @IsGranted("ROLE_USER")
     */
    public function form(Request $request, EntityManagerInterface $em, ?Adoption $adoption = null): Response
    {
        if (empty($adoption)) {
            $adoption = new Adoption();
        }

        $form = $this->createForm(AdoptionType::class, $adoption);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getUser();
            $adoption->setUser($user);
            $em->persist($adoption);
            $em->flush();

            return $this->redirectToRoute('adoption_index');
        }

        return $this->render('adoption/form.html.twig', [
            'adoption' => $adoption,
            'form'     => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="adoption_show")
     */
    public function show(Adoption $adoption): Response
    {
        if ($adoption->isClosed()) {
            $this->addFlash('danger', 'Cette annonce a été pourvue et tous ses chiens ont maintenant une famille !');

            return $this->redirectToRoute('adoption_index');
        }

        return $this->render('adoption/show.html.twig', [
            'adoption' => $adoption,
        ]);
    }
}
