<?php

namespace App\Controller\Admin;

use App\Entity\Adoption;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AdoptionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Adoption::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'),
            TextareaField::new('description')->hideOnIndex(),
            AssociationField::new('dogs'),
            AssociationField::new('contacts'),
        ];
    }
}
