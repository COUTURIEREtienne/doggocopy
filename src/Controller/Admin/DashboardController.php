<?php

namespace App\Controller\Admin;

use App\Entity\Adoption;
use App\Entity\Breed;
use App\Entity\Contact;
use App\Entity\Dog;
use App\Entity\User;
use App\Repository\AdoptionRepository;
use App\Repository\ContactRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /**
     * @var AdoptionRepository
     */
    private $adoptionRepository;

    public function __construct(ContactRepository $contactRepository, AdoptionRepository $adoptionRepository)
    {
        $this->contactRepository = $contactRepository;
        $this->adoptionRepository = $adoptionRepository;
    }


    /**
     * @Route("/admin", name="admin_dashboard")
     */
    public function index(): Response
    {
        $lastMessages = $this->contactRepository->findBy([], [
            'createdAt' => 'DESC',
        ], 2);

        $lastAdoptions = $this->adoptionRepository->findBy([], [
            'createdAt' => 'DESC',
        ], 2);

        return $this->render('admin/dashboard.html.twig', [
            'lastAdoptions' => $lastAdoptions,
            'lastMessages' => $lastMessages,
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('SymfonyDoggies')
            ->renderContentMaximized()
        ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Messages', 'fas fa-envelope', Contact::class);
        yield MenuItem::linkToCrud('Annonces', 'fas fa-dog', Adoption::class);
        yield MenuItem::linkToCrud('Chiens', 'fas fa-dog', Dog::class);
        yield MenuItem::linkToCrud('Races', 'fas fa-dog', Breed::class);
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-user-shield', User::class)->setPermission('ROLE_SUPER_ADMIN');
    }
}
