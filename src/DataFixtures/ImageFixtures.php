<?php

namespace App\DataFixtures;

use App\Entity\Image;
use App\Repository\DogRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ImageFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var DogRepository
     */
    private $dogRepository;

    public function __construct(DogRepository $dogRepository)
    {
        $this->dogRepository = $dogRepository;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $dogs = $this->dogRepository->findAll();


        $image = new Image();
        $image->setPath('http://www.rescue-forum.com/fichiers/archives-179/jack-chiot-croise-beauceron-x-berger-australien-association-baikal-40-3-1-.jpg-363317d1497010771');
        $image->setDog($dogs[0]);
        $manager->persist($image);

        $image2 = new Image();
        $image2->setPath('http://berger-australien.org/photo/images/albums/Standard/Standard_corps_fiston.jpg');
        $image2->setDog($dogs[1]);
        $manager->persist($image2);

        $image3 = new Image();
        $image3->setPath('https://jardinage.lemonde.fr/images/dossiers/2017-04/chiot-berger-australien-181013.jpg');
        $image3->setDog($dogs[1]);

        $manager->persist($image3);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            DogFixtures::class,
        ];
    }
}