<?php

namespace App\DataFixtures;

use App\Entity\Breed;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BreedFixtures extends Fixture
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $beauceron = new Breed();
        $beauceron->setName('Beauceron');
        $beauceron->setDescription("Le Beauceron, ou Berger de Beauce-Bas Rouge, est un chien de berger français de plaine. Son physique impressionnant repousse les plus téméraires. Cependant, derrière ses airs de chien « dissuasif », il se révèle être un formidable compagnon de vie. Ses principales qualités sont la fidélité et la responsabilité. Il se montre loyal envers son maître, qu’il protègera contre tout. Méfiant envers les étrangers, il s’agit d’un chien adorable, affectueux, doux et docile avec sa famille adoptive. Chien de garde par excellence, le Beauceron est un fabuleux chien de travail.");

        $manager->persist($beauceron);

        $bergerAustralien = new Breed();
        $bergerAustralien->setName('Berger australien');
        $bergerAustralien->setDescription("Le Berger Australien est un chien affectueux qui ne manque pas de caractère. Chien bâti pour l’activité et le travail, le Berger Australien est doté de toutes les aptitudes physiques et mentales pour remplir diverses missions. De taille moyenne, agile et intelligent, il s’intègre parfaitement à la vie de famille et s’adapte à une variété de cadres de vie, à condition de ne pas le laisser s’ennuyer. Depuis quelques années, cette race connaît un grand succès dans le coeur des Français.");

        $manager->persist($bergerAustralien);
        $manager->flush();
    }
}