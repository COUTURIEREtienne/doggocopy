<?php

namespace App\DataFixtures;

use App\Entity\Adoption;
use App\Repository\DogRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AdoptionFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var DogRepository
     */
    private $dogRepository;

    public function __construct(DogRepository $dogRepository)
    {
        $this->dogRepository = $dogRepository;
    }

    public function load(ObjectManager $manager)
    {
        $dogs = $this->dogRepository->findAll();

        $adoption = new Adoption();
        $adoption->setName('Un chien trop mignon !');
        $adoption->setDescription('Mais adoptez-le !');

        $adoption->addDog($dogs[0]);
        $manager->persist($dogs[0]);

        $manager->persist($adoption);

        $adoption2 = new Adoption();
        $adoption2->setName('Un chien vraiment trop mignon !');
        $adoption2->setDescription('Adoptez-le !');

        $adoption2->addDog($dogs[1]);
        $manager->persist($dogs[1]);
        $manager->persist($adoption);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            DogFixtures::class,
        ];
    }
}
