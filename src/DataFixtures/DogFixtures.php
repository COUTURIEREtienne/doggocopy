<?php

namespace App\DataFixtures;

use App\Entity\Dog;
use App\Repository\BreedRepository;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class DogFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var BreedRepository
     */
    private $breedRepository;

    public function __construct(BreedRepository $breedRepository)
    {
        $this->breedRepository = $breedRepository;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $beauceron = $this->breedRepository->findOneBy([
            'name' => 'Beauceron',
        ]);
        $bergerAustralien = $this->breedRepository->findOneBy([
            'name' => 'Berger australien',
        ]);

        $date = new DateTime('2018-05-04');

        $luc = new Dog();
        $luc->setName('Luc Skycâlineur');
        $luc->setDescription('Ce chien est trop mignon !');
        $luc->addBreed($beauceron);
        $luc->addBreed($bergerAustralien);
        $luc->setBirthDate($date);

        $manager->persist($luc);

        $date = new DateTime('2015-04-03');

        $megara = new Dog();
        $megara->setName('Mégara');
        $megara->setDescription('Elle a la forme, elle court partout ! Courez avec elle !');
        $megara->addBreed($bergerAustralien);
        $megara->setBirthDate($date);

        $manager->persist($megara);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            BreedFixtures::class,
        ];
    }
}